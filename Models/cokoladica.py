from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float

engine = create_engine('sqlite:///Assets/Cokoladice.db')
Session = sessionmaker(bind=engine)
Base = declarative_base()
session = Session()

class Cokoladica(Base):
    __tablename__ = "Cokoladica"
    SerijskiBroj = Column(Integer, primary_key=True, autoincrement=True)
    proizvodac = Column(String)
    naziv = Column(String)
    okus = Column(String)
    gramaza = Column(Integer)
    cijena = Column(Float)

    def ciatajBazu():
        Base.metadata.create_all(engine)
        cokoladice = session.query(Cokoladica).all()
        return cokoladice

    def brisiizBaze(serijskibroj):
        session.query(Cokoladica).filter_by(SerijskiBroj=serijskibroj).delete()
        session.commit()
    
    def dodajuBazu(proizvodac, naziv, okus, gramaza, cijena):
        cokoladica = Cokoladica(proizvodac=proizvodac, naziv=naziv, okus=okus, gramaza=gramaza, cijena=cijena)
        session.add(cokoladica)
        session.commit()
        