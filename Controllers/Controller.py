from Views.cokoladica_main import MainWindow
from Views.prikazCokoladica import PrikazCokoladica
from Views.brisnajeCokoladica import BrisanjeCokoladica
from Views.statistikaCokoladica import statistikaCokoladica

class Controler:
    def __init__(self):
        self.unosPogled = MainWindow(self.unosViews, self.prikazViews, self.brisiViews, self.statistikaVeiws)
        self.prikazPogeld = PrikazCokoladica(self.unosViews)
        self.izbrisiPogled = BrisanjeCokoladica(self.unosViews)
        self.unosPogled.update_tablice.connect(self.prikazPogeld.prikazCokoladice)
        self.unosPogled.update_tablice.connect(self.izbrisiPogled.tabilcaCokoladica)
        self.izbrisiPogled.update_tablice.connect(self.prikazPogeld.prikazCokoladice)
        self.unosViews()

    def unosViews(self):
        self.unosPogled.show()
        self.prikazPogeld.hide()
        self.izbrisiPogled.hide()

    def prikazViews(self):
        self.unosPogled.hide()
        self.prikazPogeld.show()
        self.izbrisiPogled.hide()

    def brisiViews(self):
        self.unosPogled.hide()
        self.prikazPogeld.hide()
        self.izbrisiPogled.show()

    def statistikaVeiws(self):
        self.unosPogled.show()
        self.prikazPogeld.hide()
        self.izbrisiPogled.hide()
        statistikaCokoladica()
