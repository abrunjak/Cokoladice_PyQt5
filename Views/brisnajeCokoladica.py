from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.cokoladica import Cokoladica
import functools

class BrisanjeCokoladica(QWidget):
    update_tablice = pyqtSignal()
    def __init__(self, izlaz):
        super().__init__()
        uic.loadUi("./Views/brisanjeCokoladice.ui", self)
        self.povtatakButton.clicked.connect(izlaz)
        self.tabilcaCokoladica()

    def tabilcaCokoladica(self):
        sve_cokoladice = Cokoladica.ciatajBazu()
        self.tableCokoladica.setRowCount(len(sve_cokoladice))
        row = 0
        for cokoladica in sve_cokoladice:
            self.tableCokoladica.setItem(row,0,QTableWidgetItem(str(cokoladica.SerijskiBroj)))
            self.tableCokoladica.setItem(row,1,QTableWidgetItem(cokoladica.proizvodac))
            self.tableCokoladica.setItem(row,2,QTableWidgetItem(cokoladica.naziv))
            self.tableCokoladica.setItem(row,3,QTableWidgetItem(cokoladica.okus))
            self.tableCokoladica.setItem(row,4,QTableWidgetItem(str(cokoladica.gramaza)))
            self.tableCokoladica.setItem(row,5,QTableWidgetItem(str(cokoladica.cijena)))
            self.delete_button = QPushButton("Izbriši")
            self.delete_button.clicked.connect(functools.partial(self.delete_dictionary, cokoladica.SerijskiBroj))
            self.tableCokoladica.setCellWidget(row,6,self.delete_button)
            row += 1
            
    def delete_dictionary(self, serijskibroj):
        Cokoladica.brisiizBaze(serijskibroj)
        self.update_tablice.emit()
        self.tabilcaCokoladica()
