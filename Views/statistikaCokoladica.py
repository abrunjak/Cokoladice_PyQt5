import numpy as np
import matplotlib.pyplot as plt
from Models.cokoladica import Cokoladica

def statistikaCokoladica():
    sve_cokoladice = Cokoladica.ciatajBazu()

    cokoladice = {}

    for cokoladica in sve_cokoladice:
        if cokoladica.proizvodac in cokoladice.keys():
            cokoladice[cokoladica.proizvodac].append(cokoladica.cijena)
        else:
            cokoladice[cokoladica.proizvodac]=[cokoladica.cijena]

    proizvodac = list(cokoladice.keys())
    cijena = []
    kolicina = []
    for cokoladica in cokoladice.values():
        kolicina.append(len(cokoladica))
        mid = sum(cokoladica)/len(cokoladica)
        cijena.append(mid)

    x_pos = np.arange(len(proizvodac))
    plt.rcParams["toolbar"] = "None"
    plt.figure("Statistika cijena")
    plt.bar(x_pos, cijena)
    plt.xticks(x_pos, proizvodac)
    for i in range(len(x_pos)):
        plt.text(i, cijena[i]//2, f"Prosječna\ncijena:\n{cijena[i]}\nKoličina:\n{kolicina[i]}", ha = "center")
    plt.title("Statistika cijena čokoladica")
    plt.ylabel("Cijena")
    plt.show()