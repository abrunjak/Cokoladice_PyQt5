from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.cokoladica import Cokoladica


class MainWindow(QWidget):
    update_tablice = pyqtSignal()
    def __init__(self, metodazaunos, metodapregleda, metodabrisanja, metodastatistika):
        super().__init__()
        uic.loadUi("./Views/cokoladica.ui", self)
        self.metodazaunos = metodazaunos
        self.unesi.clicked.connect(self.gumbUnos)
        self.statistika.clicked.connect(metodastatistika)
        self.ispisi.clicked.connect(metodapregleda)
        self.brisanje.clicked.connect(metodabrisanja)
        self.izadi.clicked.connect(self.close)

    def gumbUnos(self):
        self.poruka = QMessageBox()
        try:
            if len(self.proizvodac.text()) <= 0:
                raise Exception("Niste unijeli proizvođača")
            if len(self.naziv.text()) <= 0:
                raise Exception("Niste unijeli naziv")
            if len(self.okus.text()) <= 0:
                raise Exception("Niste unijeli okus")
            if len(self.gramaza.text()) > 0:
                if int(self.gramaza.text()) <= 0:
                    raise Exception("Niste unijeli ste nevažeću gramažu")
            else:
                raise Exception("Niste unijeli gramazu")
            if len(self.cijena.text()) > 0:
                if float(self.cijena.text()) < 0:
                    raise Exception("Niste unijeli ste nevažeću cijenu")
            else:
                raise Exception("Niste unijeli cijenu")           
                
            Cokoladica.dodajuBazu(self.proizvodac.text(), self.naziv.text(), self.okus.text(), self.gramaza.text(), self.cijena.text())
            self.poruka.setText(f"Uspiješno je unesena Čokoladica:\nProizvođač: {self.proizvodac.text()}\nNaziv: {self.naziv.text()}\nOkus: {self.okus.text()}\nGramaža: {self.gramaza.text()}\nCijena: {self.cijena.text()}")
            self.poruka.setIcon(QMessageBox.Information)
            self.poruka.show()
            self.proizvodac.clear()
            self.naziv.clear()
            self.okus.clear()
            self.gramaza.clear()
            self.cijena.clear()
            self.update_tablice.emit()
        except Exception as exc:
            self.poruka.setText(str(exc))
            self.poruka.setIcon(QMessageBox.Warning)
            self.poruka.show()
        
