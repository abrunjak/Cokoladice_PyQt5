from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.cokoladica import Cokoladica

class PrikazCokoladica(QWidget):
    def __init__(self, izlaz):
        super().__init__()
        uic.loadUi("./Views/prikazCokoladica.ui", self)
        self.povtatakButton.clicked.connect(izlaz)
        self.prikazCokoladice()

    def prikazCokoladice(self):
        sve_cokoladice = Cokoladica.ciatajBazu()
        self.tableCokoladica.setRowCount(len(sve_cokoladice))
        row = 0
        for cokoladica in sve_cokoladice:
            self.tableCokoladica.setItem(row,0,QTableWidgetItem(str(cokoladica.SerijskiBroj)))
            self.tableCokoladica.setItem(row,1,QTableWidgetItem(cokoladica.proizvodac))
            self.tableCokoladica.setItem(row,2,QTableWidgetItem(cokoladica.naziv))
            self.tableCokoladica.setItem(row,3,QTableWidgetItem(cokoladica.okus))
            self.tableCokoladica.setItem(row,4,QTableWidgetItem(str(cokoladica.gramaza)))
            self.tableCokoladica.setItem(row,5,QTableWidgetItem(str(cokoladica.cijena)))
            row += 1